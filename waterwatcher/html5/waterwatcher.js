/* For JSLint */
/*global alert, console, Highcharts, openDatabase, localStorage */
/* end for JSLint */
var db;
var chart;
var glassesGoal;

var DEFAULT_GLASSES_GOAL = 8;

//--------------------------------------------------------------------------------
function updateChart(actual, ideal) {
	chart.series[0].setData([Number(glassesGoal)]);
	chart.series[1].setData([actual]);
}

//--------------------------------------------------------------------------------
function mainRefresh() {
	loadWater();
}

//---------------------------------------------------------------
function createChart(actual, ideal) {

	var options = {
		chart : {
			renderTo : 'container',
			type : 'bar',
			borderWidth : 2,
			borderRadius : 0
		},
		title : {
			text : 'Water consumed',
			align: 'left'
		},
		xAxis : {
			categories : ['Glasses'],
			title : {
				text : null
			},
			min : 0
		},
		yAxis : {
			gridLineWidth : 0,
			allowDecimals : false,
			title : {
				text : '8 oz Glasses'
				// align: 'high'
			}
		},
		plotOptions : {
			bar : {
				dataLabels : {
					enabled : false
				}
			}
		},
		legend : {
			layout : 'vertical',
			align : 'right',
			verticalAlign : 'top',
			// x: -100,
			// y: 100,
			floating : true,
			borderWidth : 1,
			backgroundColor : '#FFFFFF',
			shadow : true
		},
		credits : {
			enabled : false
		},
		series : [{
			name : "Ideal",
			color : '#00FFFF',
			data : [ideal]
		}, {
			name : "Actual",
			data : [actual]
		}]
	};
	chart = new Highcharts.Chart(options);

}

//--------------------------------------------------------------------------------
function dbOpen() {

	var shortName = 'WaterWatcher';
	var version = '1.0';
	var displayName = 'WaterWatcher';
	var maxSize = 65536;
	db = openDatabase(shortName, version, displayName, maxSize);
}

//--------------------------------------------------------------------------------
function dbInitAfterOpen() {

	try {
		db.transaction(function(transaction) {
			transaction.executeSql('CREATE TABLE IF NOT EXISTS entries ' + ' (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' + ' epochdate INTEGER NOT NULL, ' + ' date TEXT NOT NULL, time TEXT NOT NULL );');
		});
	} catch (err) {
		alert("DB Create Error");
	}

	//Setup the goal glasses of water
	if (localStorage.glassesGoal) {
		glassesGoal = localStorage.glassesGoal;
	} else {
		try {
			localStorage.removeItem('glassesGoal');
			localStorage.setItem('glassesGoal', DEFAULT_GLASSES_GOAL);
			glassesGoal = DEFAULT_GLASSES_GOAL;
		} catch (err) {
			alert("Unable to write to local storage.  Is your browswer in privacy mode?")
			glassesGoal = DEFAULT_GLASSES_GOAL;
		}
	}

}

//--------------------------------------------------------------------------------
function errorHandler(transaction, error) {
	alert('DB Error: ' + error.message + ' (Code ' + error.code + ')');
	return true;
}

//--------------------------------------------------------------------------------
function clearOldData() {
	var date = new Date();
	var epochcutoffDate;
	var epochcutoffdate;

	date.setDate(date.getDate() - 7);
	epochcutoffDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
	epochcutoffdate = date.getTime() / 1000;

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries where epochdate < ?;', [epochcutoffdate], function(transaction, result) {
			for (var i = 0; i < result.rows.length; i++) {
				var row = result.rows.item(i);
				removeWaterByID(row.id);
			}

		}, errorHandler);
	});

}
//--------------------------------------------------------------------------------
/**
 * Shows the award graphic
 */

function showAward()
{
	// console.log("Showing award");
	// $('#award-png').css('visibility','visible');
	// $('#award-png').fadeIn('slow', function() {
        // // Animation complete
      // });
//       
       // $('#award-png').css('opacity','1.0');
       
        $('#award-png').animate(
        	{opacity: 1.0}, 
        	1000, 
        	function() {
    			// Animation complete.
  		});
 
}
//--------------------------------------------------------------------------------
/**
 * Hides the award graphic
 */
function hideAward()
{
// 	
	// $('#award-png').fadeOut('slow', function() {
        // // Animation complete
      // });
      
       // $('#award-png').css("opacity","0.1");
       // $('#award-png').css('opacity','0.2');
		// console.log("Hiding award");
		
		$('#award-png').animate(
        	{opacity: 0.0}, 
        	500, 
        	function() {
    			// Animation complete.
  		});
}

//--------------------------------------------------------------------------------
/**
 * Updates the displayed count of water consumed today and yesterday
 */
function updateCount() {
	var date = new Date();
	var yesterdate = new Date();
	var today = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();

	yesterdate.setDate(yesterdate.getDate() - 1);
	var yesterday = yesterdate.getMonth() + 1 + '/' + yesterdate.getDate() + '/' + yesterdate.getFullYear();

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [today], function(transaction, result) {
			$('#count').text('Today: ' + result.rows.length);
			updateChart(result.rows.length, 8);
			if (result.rows.length >= glassesGoal)
			{
				showAward();
			}
			else {
				hideAward();
			}
		}, errorHandler);
	});

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [yesterday], function(transaction, result) {
			$('#ydaycount').text('Yesterday: ' + result.rows.length);
		}, errorHandler);
	});

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [today], function(transaction, result) {
			if (result.rows.length > 0) {
				var when = result.rows.item(0).time;
				$('#lastwater').text('Last water consumption was ' + when);
			} else {
				$('#lastwater').text('');
			}
		}, errorHandler);
	});

}

//--------------------------------------------------------------------------------
/**
 * Loads the water information into the UI
 */
function loadWater() {
	var date = new Date();
	var currentDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();

	$('.entry').remove();
	//While we are here, purge old DB entries
	clearOldData();
	updateCount();

}

//--------------------------------------------------------------------------------
/**
 * Removes a water row in the database based on the ID
 * @param {integer} id ID # of the row in the database to remove
 */
function removeWaterByID(id) {
	db.transaction(function(transaction) {
		transaction.executeSql('DELETE FROM entries WHERE id=?;', [id], function() {

		}, errorHandler);
	});
}

//-------------------------------------------------------------------------------
/**
 * Removes a class of water
 *
 */
function removeWater() {
	var date = new Date();
	var currentDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [currentDate], function(transaction, result) {
			if (result.rows.length > 0) {
				var id_to_remove = result.rows.item(0).id;
				removeWaterByID(id_to_remove);
				updateCount();
			}
		}, errorHandler);
	});
}

//--------------------------------------------------------------------------------
/**
 * Saves a class of water in the database stamping it with the current time
 */
function saveWater() {
	// Compute current state and time
	var date = new Date();
	today = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
	// now   =  date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	var hour = date.getHours();
	var ampm = ' AM';
	var newhour = 0;

	if (hour > 12) {
		newhour = hour - 12;
		ampm = ' PM';
	} else if (hour == 12) {
		newhour = 12;
		ampm = ' PM';
	} else if (hour == 0) {
		newhour = 12;
	} else {
		newhour = hour;
	}

	var epochnow = date.getTime() / 1000;

	var now = newhour;

	if (date.getMinutes() < 10) {
		now = now + ':0' + date.getMinutes();
	} else {
		now = now + ':' + date.getMinutes();
	}

	now = now + ampm;

	//Add current date and time to database
	db.transaction(function(transaction) {
		transaction.executeSql('INSERT INTO entries (epochdate,date,time) VALUES (?,?,?);', [epochnow, today, now], function() {
			loadWater();
		}, errorHandler);
	});
}

//--------------------------------------------------------------------------------
/**
 * Resets the database to its initial form
 */
function dbReset() {

	db.transaction(function(transaction) {
		transaction.executeSql('DROP TABLE entries;', [], function() {

			dbInitAfterOpen();
			//			loadWater();
			jqtouch.goBack();
		}, errorHandler);
	});

}
//--------------------------------------------------------------------------------
function  checkForNewReleaseViaNewManifest()
{

  window.applicationCache.addEventListener('updateready', function(e) {
    if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
      // Browser downloaded a new app cache.
      // Swap it in and reload the page to get the new hotness.
      window.applicationCache.swapCache();
      if (confirm('A new version of this app is available. Load it?')) {
        window.location.reload();
      }
    } else {
      // Manifest didn't changed. Nothing new to server.
    }
  }, false);


}

//--------------------------------------------------------------------------------
function onBodyLoad() {
          
	//Init the database
	dbOpen();
	dbInitAfterOpen();
	
	checkForNewReleaseViaNewManifest();

	//-- Register handlers for adding water
	$('#addglass a').click(function() {

		saveWater();
	});

	//-- Register handlers for removing water
	$('#removeglass a').click(function() {

		removeWater();
	});

	//------ Register handler for DB reset
	$('#settings a').click(function() {
		if ($(this).attr('id') == "reset") {
			dbReset();
		}
	});

	$('#newthisversion a').click(function() {
		$('#newthisversion').hide();
	})
	//-- Populate screen
	loadWater();

	createChart(0, 8);

	//Refresh the main screen whenever we return to it from about screen
	$('#home').bind('pageAnimationStart', function(event, info) {
		if (info.direction == 'in')
			mainRefresh();
	});

	document.addEventListener("resume", mainRefresh, false);

	$('#goalform').submit(function() {

		glassesGoal = Number($('#goal').val());
		localStorage.glassesGoal = glassesGoal;
	});
	//
	//Set the current water goal when we enter the about page
	$('#settings').bind('pageAnimationStart', function(event, info) {
		$('#goal').val(Number(glassesGoal));

	});
	
}

