#echo "Copying min files to deploy directory"
#cp *-min.* ./deploy/

echo "Generating deployment manifest"
now=$(date)
rm ./offline.manifest
cat ./offline.manifest.src >> ./offline.manifest
echo "# $now"  >> ./offline.manifest

