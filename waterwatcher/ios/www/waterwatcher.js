var db
var chart
var glassesGoal
//--------------------------------------------------------------------------------
function appInit() {
	var gLastAdLoadedDate = null;
	// var gTotalAdsLoaded = 0;
	var gTimerId = null;
}

//--------------------------------------------------------------------------------
function onBodyLoad() {
	console.log("onBodyLoad");
	document.addEventListener("deviceready", onDeviceReady, false);

	//Init the database
	dbOpen();
	dbInitAfterOpen();

	//-- Register handlers for adding water
	$('#addglass a').click(function() {
		console.log("Adding a glass water");
		saveWater();
	});
	
	//-- Register handlers for removing water
	$('#removeglass a').click(function() {
		console.log("Removing a glass water");
		removeWater();
	});

	//------ Register handler for DB reset
	$('#about a').click(function() {
		if ($(this).attr('id') == "reset") {
			dbReset();
		}
	});
	
	$('#newthisversion a').click(function() {
		$('#newthisversion').hide();
	})

	//-- Populate screen
	loadWater();
	
	createChart(0,8);
	
	//Refresh the main screen whenever we return to it from about screen
	$('#home').bind('pageAnimationStart', function(event, info){ 
    if (info.direction == 'in') 
        mainRefresh();
	});
	
	document.addEventListener("resume",mainRefresh,false);
	
	 $('#goalform').submit(function(){
	 		console.log("New goal value is " + Number($('#goal').val()));
	 		glassesGoal = Number($('#goal').val());
	 		localStorage.glassesGoal = glassesGoal;
     });
//      
     //Set the current water goal when we enter the about page
     $('#settings').bind('pageAnimationStart',function(event,info) {
     	$('#goal').val(Number(glassesGoal));
     	
     });

}
//--------------------------------------------------------------------------------
function updateChart(actual,ideal)
{
	chart.series[0].setData([Number(glassesGoal)]);
	chart.series[1].setData([actual]);
}
//--------------------------------------------------------------------------------
function mainRefresh()
{
	loadWater();
}
//---------------------------------------------------------------
function createChart(actual, ideal) {
	
	
		
	var options = {
            chart: {
                renderTo: 'container',
                type: 'bar',
                // height: 200,
                // width:320,
                borderWidth:2,
                borderRadius:0
            },
            title: {
                text: 'Water consumed'
            },
            // subtitle: {
                // text: ''
            // },
            xAxis: {
                categories: ['Glasses' ],
                title: {
                    text: null
                },
                min:0
            },
            yAxis: {
                gridLineWidth: 0,
                allowDecimals : false,
                // categories: [""],
                title: {
                    text: '8 oz Glasses'
                    // align: 'high'
                }
            },
            // tooltip: {
                // formatter: function() {
                    // return ''+
                        // this.series.name +': '+ this.y +' millions';
                // }
            // },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                // x: -100,
                // y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [
               	{
                	name: "Ideal",
                	color: '#00FFFF',
                	data: [ideal]
            	},
            	{
                	name: "Actual",
                	data: [actual]
            	}
         ]
        };
 
		// options.series[0].data = allVisits;
        // options.series[1].data = newVisitors;	
		chart = new Highcharts.Chart(options);
		
					
}

//--------------------------------------------------------------------------------
function onOrientationChange() {
	//alert(window.orientation);
}

//--------------------------------------------------------------------------------
/* When this function is called, PhoneGap has been initialized and is ready to roll */
function onDeviceReady() {

	// listen for orientation changes

	window.addEventListener("orientationchange", window.plugins.iAdPlugin.orientationChanged, false);

	window.plugins.iAdPlugin.orientationChanged(true);
	//trigger immediately so iAd knows its orientation on first load

	window.plugins.iAdPlugin.showAd(true);
	// listen for the "iAdBannerViewDidLoadAdEvent" that is sent by the iAdPlugin
	document.addEventListener("iAdBannerViewDidLoadAdEvent", iAdBannerViewDidLoadAdEventHandler, false);
	// listen for the "iAdBannerViewDidFailToReceiveAdWithErrorEvent" that is sent by the iAdPlugin
	document.addEventListener("iAdBannerViewDidFailToReceiveAdWithErrorEvent", iAdBannerViewDidFailToReceiveAdWithErrorEventHandler, false);

	var adAtBottom = false;
	setTimeout(function() {
		window.plugins.iAdPlugin.prepare(adAtBottom);
		// by default, ad is at Top
	}, 1000);

}

//--------------------------------------------------------------------------------
function iAdBannerViewDidFailToReceiveAdWithErrorEventHandler(evt) {
	console(evt.error);
	window.plugins.iAdPlugin.showAd(false);
}

//--------------------------------------------------------------------------------
function iAdBannerViewDidLoadAdEventHandler(evt) {

	window.plugins.iAdPlugin.showAd(true);

}

//--------------------------------------------------------------------------------
function showAdClicked(evt) {
	window.plugins.iAdPlugin.showAd(evt.checked);
}

//--------------------------------------------------------------------------------
function dbOpen() {

	var shortName = 'WaterWatcher';
	var version = '1.0';
	var displayName = 'WaterWatcher';
	var maxSize = 65536;
//	closeDatabase(db);
	db = openDatabase(shortName, version, displayName, maxSize);
	console.log("dbOpen()");
}

//--------------------------------------------------------------------------------
function dbInitAfterOpen() {
  console.log("dbInitAfterOpen()");
	db.transaction(function(transaction) {
								 transaction.executeSql('CREATE TABLE IF NOT EXISTS entries ' + ' (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' + ' epochdate INTEGER NOT NULL, ' + ' date TEXT NOT NULL, time TEXT NOT NULL );');
								 // transaction.executeSql('CREATE TABLE IF NOT EXISTS entries ' + ' (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ' + ' date DATE NOT NULL, time TEXT NOT NULL );');
								 });
						
	//Setup the goal glasses of water
	if (localStorage.glassesGoal)
	{
		glassesGoal = localStorage.glassesGoal;
	}
	else
	{
		localStorage.glassesGoal=8;
		glassesGoal = 8;
	}

}

//--------------------------------------------------------------------------------
function errorHandler(transaction, error) {
	alert('DB Error: ' + error.message + ' (Code ' + error.code + ')');
	return true;
}

//--------------------------------------------------------------------------------
function clearOldData() {
	var date = new Date();
	
    date.setDate(date.getDate() - 7);
	var epochcutoffDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
	var epochcutoffdate = date.getTime()/1000;

	console.log("old data date = " +epochcutoffDate);
	console.log("old data epoch time =" + epochcutoffdate);

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries where epochdate < ?;',[epochcutoffdate],function(transaction,result) {
			for (var i = 0; i < result.rows.length; i++) {
				var row = result.rows.item(i);
				console.log("clearning entry: "+row.id + " "+ row.date + " " +row.time+" "+row.epochdate);
				removeWaterByID(row.id);
			}
			
		},errorHandler);
	});

}

//--------------------------------------------------------------------------------
function insertWaterEntry(when, entryID) {
	//This approach seems to work better than the clone method I would use to clone an invisible
	//template entry in query
	console.log("insertWaterEntry");
	var html = '';
	// html += '<div class="aglass" id="glass' + entryID + '">'
	// html += '   <ul class="edgetoedge ">'
	html += '   <li class="entry" id="entry' + entryID + '">'
	html += '   <img src="water-drop-29x21.png" align="center"></img>'
	// html += '      <span class="label">Glass of water at ' + when + ' </span>'
	html += '      <span class="timestamp">at ' + when + ' </span>'
	html += '      <span id="' + entryID + '" class="button">Delete</span>'
	html += '   </li>'
	// html += '   </ul>'
	// html += '   </div>'

	console.log("Inserting");
	// $('#glasses').append(html);
	$('.aglass').append(html);

}

//--------------------------------------------------------------------------------
/**
 * Updates the displayed count of water consumed today and yesterday
 */
function updateCount() {
	var date = new Date();
	var yesterdate = new Date();
	var today = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
	console.log("updateCount()");
	
	yesterdate.setDate(yesterdate.getDate() - 1);
	var yesterday = yesterdate.getMonth() + 1 + '/' + yesterdate.getDate() + '/' + yesterdate.getFullYear();
	
	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [today], function(transaction, result) {
			$('#count').text('Today: ' + result.rows.length);
			updateChart(result.rows.length,8);		
		}, errorHandler);
	});

	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [yesterday], function(transaction, result) {
			$('#ydaycount').text('Yesterday: ' + result.rows.length);
		}, errorHandler);
	});
	
	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [today], function(transaction, result) {
			if (result.rows.length > 0) {
				var when = result.rows.item(0).time;
				$('#lastwater').text('Last water consumption was '+ when);
			}
			else
			{
				$('#lastwater').text('');
			}
		}, errorHandler);
	});		
	
	

}


//--------------------------------------------------------------------------------
/**
 * Loads the water information into the UI
 */
function loadWater() {
	var date = new Date();
	var currentDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();

	console.log("Loading water data");
	// Clear the existing entries
	// $('.aglass').remove();
	$('.entry').remove();
	//While we are here, purge old DB entries
	clearOldData();

	//RJG - this provides a detailed list of woter entries.  Removed to clean up UI
	// console.log("Looking for entries for date "+currentDate);
	// db.transaction(function(transaction) {
		// transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [currentDate], function(transaction, result) {
			// for (var i = 0; i < result.rows.length; i++) {
				// var row = result.rows.item(i);
				// insertWaterEntry(row.time, row.id);
				// //Install handler for deletes
				// $('#' + row.id).click(function() {
					// entry = $(this).attr('id');
					// //Whack the DB entry
					// removeWater(entry);
					// // $('#glass' + entry).remove();
					// $('#entry' + entry).remove();
					// updateCount();
// 
				// });
			// }
		// }, errorHandler);
	// });
	updateCount();

}

//--------------------------------------------------------------------------------
/**
 * Removes a water row in the database based on the ID
 * @param {integer} id ID # of the row in the database to remove
 */
function removeWaterByID(id) {
	db.transaction(function(transaction) {
		transaction.executeSql('DELETE FROM entries WHERE id=?;', [id], function() {
			console.log('Removed water entry ' + id)
		}, errorHandler);
	});
}

//-------------------------------------------------------------------------------
/**
 * Removes a class of water
 * 
 */
function removeWater()
{
	var date = new Date();
	var currentDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
	
	console.log("removeWater()");
	db.transaction(function(transaction) {
		transaction.executeSql('SELECT * FROM entries WHERE date = ? ORDER BY id DESC;', [currentDate], function(transaction, result) {
			if (result.rows.length > 0) {
				var id_to_remove = result.rows.item(0).id;
				removeWaterByID(id_to_remove);
				updateCount();
			}
			// for (var i = 0; i < result.rows.length; i++) {
				// var row = result.rows.item(i);
				// removeWater(row.id);
				// break;
			// }
		}, errorHandler);
	});		
}
//--------------------------------------------------------------------------------
/**
 * Saves a class of water in the database stamping it with the current time
 */
function saveWater() {
	// Compute current state and time
	var date = new Date();
	today = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
	// now   =  date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	var hour = date.getHours();
	var ampm = ' AM';
	var newhour = 0;
	if (hour > 12) {
		newhour = hour - 12;
		ampm = ' PM';
	} else if (hour == 0) {
		newhour = 12;
	} else {
		newhour = hour;
	}
	
	var epochnow=date.getTime()/1000;

	var now = newhour;

	if (date.getMinutes() < 10) {
		now = now + ':0' + date.getMinutes();
	} else {
		now = now + ':' + date.getMinutes();
	}

	now = now + ampm;

	//	now = date.getHours() + ':' + date.getMinutes();
	//alert(now);
	
	console.log("Adding entry " + today + " " + now);

	//Add current date and time to database
	db.transaction(function(transaction) {
		transaction.executeSql('INSERT INTO entries (epochdate,date,time) VALUES (?,?,?);', [epochnow,today, now], function() {
			loadWater();
		}, errorHandler);
	});
}

//--------------------------------------------------------------------------------
/**
 * Resets the database to its initial form
 */
function dbReset() {
	console.log("resetting database");
	db.transaction(function(transaction) {
		transaction.executeSql('DROP TABLE entries;', [], function() {
			console.log("recreate DB");
			dbInitAfterOpen();
//			loadWater();
			jqtouch.goBack();
		}, errorHandler);
	});
	

}


